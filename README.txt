
Field Roundabout
===============

The Field Roundabout module creates a field formatter using the jQuery
Roundabout plugin.


Installation
===============

1) Install the Libraries API module http://www.drupal.org/project/libraries
2) Create a sites/all/libraries directory on your server
3) Create a jquery.roundabout directory within sites/all/libraries
4) Download the jQuery Roundabout plugin http://fredhq.com/projects/roundabout
5) Copy jquery.roundabout.min.js or jquery.roundabout.js to the
   jquery.roundabout directory. When done, you should have
   sites/all/libraries/jquery.roundabout/jquery.roundabout.min.js
6) Create a jquery.roundabout-shapes within sites/all/libraries
7) Download the jQuery Roundabout Shapes plugin
   http://fredhq.com/projects/roundabout-shapes
8) Copy jquery.roundabout-shapes.min.js or jquery.roundabout-shapes.js to the
   jquery.roundabout-shapes directory. When done, you should have
   sites/all/libraries/jquery.roundabout-shapes/jquery.roundabout-shapes.min.js


Requirements
===============
* Libraries
