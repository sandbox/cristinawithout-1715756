(function ($) {
  Drupal.behaviors.field_roundabout = {
    attach: function(context, settings) {

      for (i in Drupal.settings.field_roundabout) {
        var settings = Drupal.settings.field_roundabout[i];
        var roundabout = $('.' + i);

        if (!roundabout.hasClass('field-roundabout-processed')) {
          roundabout.addClass('field-roundabout-processed');
          var options = {};

          if(settings.shape) {
            options.shape = settings.shape;
          }
          if(settings.autoplay) {
            options.autoplay = settings.autoplay
            if(settings.autoplayDuration !== '') {
              options.autoplayDuration = parseInt(settings.autoplayDuration);
            }
            if(settings.autoplayPauseOnHover) {
              options.autoplayPauseOnHover = settings.autoplayPauseOnHover;
            }
          }
          if(settings.duration !== '') {
            options.duration = parseInt(settings.duration);
          }

          $(roundabout).roundabout(options);
        }
      }

    }
  };

})(jQuery);
