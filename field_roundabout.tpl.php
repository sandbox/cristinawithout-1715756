<?php
/**
 * @file
 * Template file for field_roundabout.
 */
?>
<div id="field-roundabout-<?php print $id; ?>-wrapper" class="field-roundabout-wrapper">

  <ul class="<?php print $classes; ?>">
    <?php foreach ($items as $num => $item) : ?>
      <li class="<?php print $item['classes']; ?>">
        <?php print $item['image']; ?>
      </li>
    <?php endforeach; ?>
  </ul>
</div>
